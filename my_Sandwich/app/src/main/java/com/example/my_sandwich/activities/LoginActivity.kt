package com.example.my_sandwich.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.my_sandwich.R
import com.example.my_sandwich.viewmodels.LoginViewModel
import com.example.my_sandwich.databinding.UserloginBinding
import kotlinx.android.synthetic.main.userlogin.*
import kotlinx.android.synthetic.main.userregister.*
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber

class LoginActivity: AppCompatActivity(), LifecycleObserver {

private lateinit var binding: UserloginBinding
private lateinit var viewModel : LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.userlogin)
     viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)


        val queue = Volley.newRequestQueue(this);
          viewModel.url

           fun login(){
               val jObj = JSONObject()
            try {
                jObj.put("email", binding.emailuser.text)
                jObj.put("passw", binding.passwuser.text)
                jObj.put("format", "json")


// wanneer de request niet lukt
            } catch (e: JSONException) {
                Toast.makeText(
                    this@LoginActivity,
                    "problemen met de server, kom later terug!",
                    Toast.LENGTH_SHORT
                ).show()
            }



            val req = JsonObjectRequest(com.android.volley.Request.Method.POST, viewModel.url, jObj,
                {
                    Toast.makeText(this, "welkom terug ! ${emailuser.text}", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, SandwichActivity::class.java))
             // bij een error
                }, {
                    Toast.makeText(this, "je gegevens zijn niet ingevuld of onjuist, probeer opnieuw :(.", Toast.LENGTH_SHORT).show()

                })

            queue.add(req)
        }







       // Navigeren naar register activity
        binding.btnRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }



        binding.goButton.setOnClickListener {
          login()

        }


    }




    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.i("background")
    }
}