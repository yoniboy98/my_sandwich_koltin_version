package com.example.my_sandwich.Adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.my_sandwich.R
import com.example.my_sandwich.Util
import com.example.my_sandwich.models.Sandwiches

// wordt gebruikt voor recycleview maar is nog niet geimplementeerd
class SandwichAdapter: RecyclerView.Adapter<Util.TextItemViewHolder>() {

var data = listOf<Sandwiches>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Util.TextItemViewHolder {

    val layoutInflater = LayoutInflater.from(parent.context)

        val view = layoutInflater
            .inflate(R.layout.list_items, parent, false) as TextView
        return Util.TextItemViewHolder(view)
    }




    override fun onBindViewHolder(holder: Util.TextItemViewHolder, position: Int) {
 val item = data[position]
        holder.textView.text = item.name
    }

    override fun getItemCount() = data.size

}