package com.example.my_sandwich.activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.my_sandwich.R
import com.example.my_sandwich.databinding.MainpageBinding
import com.example.my_sandwich.viewmodels.SandwichViewModel
import kotlinx.android.synthetic.main.mainpage.*
import kotlinx.android.synthetic.main.userlogin.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class SandwichActivity : AppCompatActivity(), LifecycleObserver {
// binding en viewmodel
    private lateinit var binding: MainpageBinding
    private lateinit var viewModel : SandwichViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.mainpage)
        viewModel = ViewModelProvider(this).get(SandwichViewModel::class.java)

        val queue = Volley.newRequestQueue(this);
    try {
        // komt uit de viewmodel
    viewModel.getJsonSandwiches()

        //wanneer het niet werkt
     }catch (e: JSONException){
    Toast.makeText(
        this@SandwichActivity,
        "problemen met de server, kom later eens terug!",
        Toast.LENGTH_SHORT
    ).show()
}


        val jsObjRequest = JsonObjectRequest(
            com.android.volley.Request.Method.POST, viewModel.urlSandwich,  viewModel.jObj,
            { response ->
                val eu = StringBuilder("euro")
                val kcal = StringBuilder("kcal")
                var tArray: JSONArray?
                var tObjectRecord: JSONObject?

                try {
                    tArray = response.getJSONArray("data")
                    for (i in 0 until tArray!!.length()) {
                        tObjectRecord = tArray.getJSONObject(i)
                      binding.sandwichname.text = tObjectRecord.getString("name").toString()
                        binding.kcal.text = tObjectRecord.getString("kcal").toInt().toString()
                        binding.available.text = tObjectRecord.getString("available").toString()
                        binding.extra.text = tObjectRecord.getString("extra").toString()
                        binding.price.text = tObjectRecord.getString("price").toDouble().toString()

                        binding.price.append(" $eu")
                        binding.kcal.append(" $kcal")

                    }
// wanneer het niet lukt
                } catch (e: JSONException) {
                   Toast.makeText(this@SandwichActivity,"kan de gegevens niet ophalen, probeer het later opnieuw",Toast.LENGTH_LONG).show()
                }
            }, { error -> binding.sandwichname.text = error.cause.toString() })

        // Add the request to the RequestQueue
        queue.add(jsObjRequest)




        binding.orderbutton.setOnClickListener {
            Toast.makeText(
                this@SandwichActivity,
                "Sorry, deze functionaliteit is voor in de toekomst!",
                Toast.LENGTH_LONG
            ).show()
        }
    }

}