package com.example.my_sandwich.viewmodels

import androidx.lifecycle.ViewModel

class RegisterViewModel: ViewModel() {

    val apiaddress = "https://myhostingname.be/"
    val url = apiaddress + "php/authentication.php"

    //regex patterns zodat de user geen vreemde data kan ingeven.

    val namePattern = Regex("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*\$")
    val emailPattern = Regex("^((\"[\\w-\\s]+\")|([\\w-]+(?:\\.[\\w-]+)*)|(\"[\\w-\\s]+\")([\\w-]+(?:\\.[\\w-]+)*))" +
            "(@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}" +
            "(?:\\.[a-z]{2})?)\$)|(@\\[?" +
            "((25[0-5]\\.|2[0-4][0-9]\\.|1[0-9]{2}\\.|[0-9]{1,2}\\.))" +
            "((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\]?\$)")
    val passwPattern = Regex("^[a-zA-Z]\\w{6,14}\$")
    val adresPattern = Regex("^(.+)\\s(\\d+)\\s?(.?)(.*)?")
    val numberPattern = Regex("^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*\$")


}
