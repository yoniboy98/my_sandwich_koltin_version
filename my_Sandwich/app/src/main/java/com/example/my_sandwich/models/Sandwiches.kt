package com.example.my_sandwich.models
// dit heeft momenteel geen functie en wordt nog niet gebruikt.
data class Sandwiches(
    val available: String,
    val broodtype: String,
    val extra: String,
    val kcal: Number,
    val name: String,
    val price: Double,
    val saus: String
)
