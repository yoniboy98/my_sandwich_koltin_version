
De nieuwe bestel app mySandwich is een feit.
Of je zin hebt in een broodje balleke of een smos, bij mySandwich is er voor ieder wat wils.
Het bestellen van jouw broodjes is daarom nu nog makkelijker en sneller.
Je wordt gratis per notificatie op de hoogte gehouden bij de voortgang van je bestelling.
Zo weet je precies op welk moment je kan verwachten dat je bestelling klaar is.
Deze bestelling kan je dan komen ophalen in onze zaak! 
Een feestmaal voor het hele gezin of een snelle hap, meteen bezorgd of gepland voor later:
Download de mySandwich app en krijg je bestelling in no time! 
