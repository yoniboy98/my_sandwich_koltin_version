package com.example.my_sandwich.models

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "klanten")
data class User (
    @ColumnInfo(name ="address")
    val address: String,
    @ColumnInfo(name ="age")
    val age: String,
    @ColumnInfo(name ="email")
    val email: String,
    @ColumnInfo(name ="family_name")
    val family_name: String,
    @ColumnInfo(name ="name")
    val name: String,
    @ColumnInfo(name ="passw")
    val passw: String,
    @ColumnInfo(name ="role")
    val role: String,
    @ColumnInfo(name ="telephone_number")
    val telephone_number: String
)