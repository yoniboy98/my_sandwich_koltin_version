package com.example.my_sandwich.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.my_sandwich.R
import com.example.my_sandwich.databinding.UserregisterBinding
import com.example.my_sandwich.viewmodels.RegisterViewModel
import kotlinx.android.synthetic.main.userlogin.*
import kotlinx.android.synthetic.main.userregister.*
import org.json.JSONException
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {

    // viewmodel en binding
    private lateinit var binding: UserregisterBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.userregister)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)

        val queue = Volley.newRequestQueue(this);
        viewModel.url

// input validation
        fun registerUser() {
            if (!binding.adresuserregister.text!!.contains(viewModel.adresPattern)) {
                Toast.makeText(this,
                    "Adres moet je eigen thuisadres zijn.",
                    Toast.LENGTH_LONG).show()

        }else if (!binding.emailuserregister.text!!.contains(viewModel.emailPattern)) {
                Toast.makeText(this,
                    "Zorg voor een logisch email adres bv(voornaam.naam@hotmail.com).",
                    Toast.LENGTH_LONG).show()

            } else if (!binding.familynameUserRegister.text!!.contains(viewModel.namePattern) or (!binding.nameUserRegister.text!!.contains(viewModel.namePattern)) )  {
                Toast.makeText(this,
                    "Je voornaam en/of achternaam.",
                    Toast.LENGTH_LONG).show()

            }else if (!binding.passworduserregister.text!!.contains(viewModel.passwPattern)) {
                Toast.makeText(this,
                    "Je wachtwoord moet minstens uit 6 tekens bestaan, geen leestekens en mag niet starten met een nummer.",
                    Toast.LENGTH_LONG).show()

            }else if (!binding.numberUserRegistration.text!!.contains(viewModel.numberPattern)) {
                Toast.makeText(this,
                    "Je eigen gsm nummer zodat we jou goed kunnen bereiken bv.(0470565378).",
                    Toast.LENGTH_LONG).show()
            }

            else {
// json object aanmaken.
            val jObj = JSONObject()
            try {
                jObj.put("bewerking", "add")
                jObj.put("table", "klanten")
                jObj.put("format", "json")
                jObj.put("address",binding.adresuserregister.text)
                jObj.put("email",binding.emailuserregister.text)
                jObj.put("family_name", binding.familynameUserRegister.text)
                jObj.put("name", binding.nameUserRegister.text)
                jObj.put("passw", binding.passworduserregister.text)
                jObj.put("role","2")
                jObj.put("telephone_number",binding.numberUserRegistration.text)



            } catch (e: JSONException) {
                Toast.makeText(
                    this@RegisterActivity,
                    "problemen met de server, kom later eens terug!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            val req = JsonObjectRequest(
                Request.Method.POST, viewModel.url, jObj,
                {
                    Toast.makeText(this, "Welkom ${familynameUserRegister.text}! :D", Toast.LENGTH_LONG).show()
                    startActivity(Intent(this, LoginActivity::class.java))
                    overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
                }, {
                    Toast.makeText(this, "Registreren is niet gelukt ! :(", Toast.LENGTH_SHORT).show()


                })

            queue.add(req)

        }}



            // Navigeren naar login activity met de animatie uit de anim package.
            btnLogRegister.setOnClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }


            binding.buttonRegister.setOnClickListener {
                registerUser()
            }



    }
}