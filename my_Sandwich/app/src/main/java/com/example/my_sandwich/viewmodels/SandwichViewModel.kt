package com.example.my_sandwich.viewmodels

import androidx.lifecycle.ViewModel
import org.json.JSONObject

class SandwichViewModel : ViewModel() {

    val apiAddress = "https://myhostingname.be/"
    val urlSandwich = apiAddress + "php/producten.php";
    val jObj = JSONObject()

    // json object
    fun getJsonSandwiches() {
        jObj.put("bewerking", "get")
        jObj.put("table", "sandwiches")
        jObj.put("format", "json")

    }
}
